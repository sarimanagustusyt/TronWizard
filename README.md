---
title: Spaceblure
emoji: 📉
colorFrom: red
colorTo: gray
sdk: Gradio_x
sdk_version: 1.10.11
app_file: app.py
pinned: false
---
